﻿# Host: localhost  (Version 5.5.5-10.1.34-MariaDB)
# Date: 2019-07-15 17:19:37
# Generator: MySQL-Front 6.0  (Build 2.20)


#
# Structure for table "acount"
#

DROP TABLE IF EXISTS `acount`;
CREATE TABLE `acount` (
  `id_acount` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_acount`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

#
# Data for table "acount"
#

INSERT INTO `acount` VALUES (1,'dwc','cwdc','2019-07-15 03:06:10','2019-07-15 03:06:10'),(2,'edgar','edgar','2019-07-15 03:06:19','2019-07-15 03:13:13'),(3,'asd','asd','2019-07-15 03:11:44','2019-07-15 03:16:44');

#
# Structure for table "client"
#

DROP TABLE IF EXISTS `client`;
CREATE TABLE `client` (
  `id_client` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `CI` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lastname` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `city` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mail` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_client`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

#
# Data for table "client"
#

INSERT INTO `client` VALUES (3,'a','a','a','a','a','a','a','2019-07-15 03:27:05','2019-07-15 03:29:20'),(4,'eqwe','weqe','we','wewe','we','we','we','2019-07-15 03:46:26','2019-07-15 03:46:26'),(5,'as','dasd','dasd','sads','asdas','dsad@gmail.com','asdas','2019-07-15 14:46:52','2019-07-15 14:46:52'),(6,'as','a','a','a','a','a','a','2019-07-15 15:11:35','2019-07-15 15:11:45');

#
# Structure for table "contacts"
#

DROP TABLE IF EXISTS `contacts`;
CREATE TABLE `contacts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `first_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `job_title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `city` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `country` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

#
# Data for table "contacts"
#

INSERT INTO `contacts` VALUES (2,'2019-07-15 01:14:59','2019-07-15 02:58:23','m','m','m','m','m','m'),(3,'2019-07-15 01:15:08','2019-07-15 01:23:00','edgar','vicente','mancilla','casa','city','contry');

#
# Structure for table "cost"
#

DROP TABLE IF EXISTS `cost`;
CREATE TABLE `cost` (
  `id_cost` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `client` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `total_cost` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `discount` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_cost`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

#
# Data for table "cost"
#

INSERT INTO `cost` VALUES (1,'asdsas','dd','Dad','ADa','2019-07-15 04:26:41','2019-07-15 04:26:41'),(2,'sdsa','asdsa','sadsad','sad','2019-07-15 04:29:45','2019-07-15 04:29:45'),(3,'s','s','s','s','2019-07-15 04:31:56','2019-07-15 15:14:18');

#
# Structure for table "detalorder"
#

DROP TABLE IF EXISTS `detalorder`;
CREATE TABLE `detalorder` (
  `id_detalorder` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `price_per_car` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `quantity` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `order_date` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `return_date` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `days` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `namber_orders` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_detalorder`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

#
# Data for table "detalorder"
#

INSERT INTO `detalorder` VALUES (1,'tb5t','t4t4','t45t','t54t','t54','t45t','2019-07-13 19:25:10','2019-07-13 19:25:10');

#
# Structure for table "migrations"
#

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=45 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

#
# Data for table "migrations"
#

INSERT INTO `migrations` VALUES (34,'2014_10_12_000000_create_users_table',1),(35,'2014_10_12_100000_create_password_resets_table',1),(36,'2019_05_29_212459_create_client_table',1),(37,'2019_05_29_213609_create_order_table',1),(38,'2019_05_29_213640_create_detalorder_table',1),(39,'2019_05_29_213702_create_cost_table',1),(40,'2019_05_29_213726_create_vehicle_table',1),(41,'2019_05_29_213740_create_model_table',1),(42,'2019_05_29_213815_create_user_table',1),(43,'2019_05_29_213844_create_acount_table',1),(44,'2019_07_13_134147_create_contacts_table',1);

#
# Structure for table "model"
#

DROP TABLE IF EXISTS `model`;
CREATE TABLE `model` (
  `id_model` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name_model` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `namber_of_seats` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `manufacturing_date` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_model`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

#
# Data for table "model"
#

INSERT INTO `model` VALUES (1,'fdaf','fadfafaf','fasf','2019-07-13 19:33:57','2019-07-13 19:33:57'),(2,'as','as','as','2019-07-15 04:44:32','2019-07-15 04:44:32'),(3,'a','a','a','2019-07-15 15:14:01','2019-07-15 15:14:01');

#
# Structure for table "order"
#

DROP TABLE IF EXISTS `order`;
CREATE TABLE `order` (
  `id_order` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name_client` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `total_price` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `detail_order` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_order`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

#
# Data for table "order"
#

INSERT INTO `order` VALUES (2,'d','d','d','d','2019-07-15 04:59:32','2019-07-15 04:59:40'),(3,'dasd','dasd','asdas','sad','2019-07-15 05:10:30','2019-07-15 05:10:30'),(4,'dsad','asdsa','sad','sad','2019-07-15 05:10:40','2019-07-15 05:10:40');

#
# Structure for table "password_resets"
#

DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

#
# Data for table "password_resets"
#


#
# Structure for table "user"
#

DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id_user` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `login` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `acount` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_user`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

#
# Data for table "user"
#

INSERT INTO `user` VALUES (1,'dd','asdsad',NULL,'2019-07-13 19:50:55','2019-07-13 19:50:55'),(2,'dd','fwef',NULL,'2019-07-13 19:53:02','2019-07-13 19:53:02'),(3,'dd','dsad',NULL,'2019-07-13 19:53:49','2019-07-13 19:53:49'),(4,'daas','fasfasf','dsad@gmail.com','2019-07-13 19:54:04','2019-07-13 19:54:04'),(5,NULL,NULL,NULL,'2019-07-13 19:58:43','2019-07-13 19:58:43');

#
# Structure for table "users"
#

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

#
# Data for table "users"
#

INSERT INTO `users` VALUES (1,'edgar','jhondx0123@gmail.com',NULL,'$2y$10$kOJaXX6LV10p42dZ9qDQ5e7NrTWEkJ.7m4mBuaykPTtICsl0I73.O',NULL,'2019-07-15 05:02:00','2019-07-15 05:02:00');

#
# Structure for table "vehicle"
#

DROP TABLE IF EXISTS `vehicle`;
CREATE TABLE `vehicle` (
  `id_vehicle` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `enrollent` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `model` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `colour` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `brand` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mileage` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `year` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_vehicle`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

#
# Data for table "vehicle"
#

INSERT INTO `vehicle` VALUES (1,'a','b','c','d','e','f','2019-07-13 19:47:20','2019-07-15 04:13:12'),(2,'41414','1414','red','nissan','7452','2019','2019-07-15 14:22:02','2019-07-15 14:22:02'),(3,'a','a','a','a','a','a','2019-07-15 15:13:32','2019-07-15 15:13:43');
